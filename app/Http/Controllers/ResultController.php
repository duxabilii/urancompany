<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResultRequest;
use App\Models\Result;
use App\Services\ResultService;
use Illuminate\Support\Facades\Auth;


class ResultController extends Controller
{
    private $resultService;

    public function __construct(ResultService $resultService)
    {
        $this->resultService = $resultService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Auth::user()
                       ->results()
                       ->paginate(20);
        return view('results.index', [
            'results' => $results
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ResultRequest $request
     * @return Result
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store(ResultRequest $request)
    {

        $result = $this->resultService->store($request);
        if (!$result) {
            return view('welcome')->withErrors(config('search.message.error_fetch'));
        }

        $format = $result->position > 0 ? config('search.message.position_found') : config('search.message.position_not_found');

        $message = sprintf(
            $format,
            $request->get('domain'),
            $request->get('keyword'),
            $result->position
        );

        return view('welcome')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Result $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result)
    {
        //
    }
}
