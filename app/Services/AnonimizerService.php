<?php
/**
 * Created by PhpStorm.
 * User: duxabilii
 * Date: 13.11.2018
 * Time: 8:28
 */

namespace App\Services;


use GuzzleHttp\Client;

class AnonimizerService
{
    /*
     * @var Client $client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $url
     * @return mixed
     */
    public function anonimize(string $url)
    {
        $data = [
            'url' => $url
        ];

        $response = $this->client->get('http://noblockme.ru/api/anonymize?' . http_build_query($data));

        $json = $response->getBody()->getContents();
        $json = json_decode($json);
        if ($json->status === 0) {
            return $json->result;
        }

    }
}