<?php

namespace App\Services;


use App\Http\Requests\ResultRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DomCrawler\Crawler;

class ParserService
{
    /*
     * @var DomCrawler $crawler
     */
    private $crawler;

    /*
     * @var ResultRequest $request
     */
    private $request;

    /*
     * @var Client $client
     */
    private $client;

    private $anonimizerService;

    public function __construct
    (
        Crawler $crawler,
        Client $client,
        AnonimizerService $anonimizerService
    ) {
        $this->client            = $client;
        $this->crawler           = $crawler;
        $this->anonimizerService = $anonimizerService;
    }


    /**
     * Return position of domain
     *
     * @param ResultRequest $request
     * @return int|string
     * @throws GuzzleException
     */
    public function parse(ResultRequest $request)
    {
        $this->setRequest($request);

        $urls = $this->getUrls();
        return $this->findPosition($urls);

    }

    /**
     * Build correct search string
     *
     * @return string
     */
    public function buildUri()
    {
        $url = config('search.search_uri');
        $url = $this->anonimizerService->anonimize($url);
        return $this->addSearchParam($url);
    }

    public function addSearchParam(string $url)
    {
        return $url . 'search';
    }

    /**
     * Get all urls from html
     *
     * @return array
     * @throws GuzzleException
     */
    private function getUrls()
    {
        $url = $this->buildUri();

        $page = $this->getPage($url);

        $this->crawler->addHtmlContent($page);
        $links = $this->crawler->filter('cite');

        $urls     = [];
        $position = 1;
        foreach ($links as $link) {
            $urls[$position++] = $link->textContent;
        }
        return $urls;
    }

    /**
     * Find position of entered url
     *
     * @param array $urls
     * @return int
     */
    private function findPosition(array $urls)
    {
        $request = $this->getRequest()->toArray();
        foreach ($urls as $position => $url) {
            if (mb_strpos($url, $request['domain']) !== false) {
                return $position;
            }
        }
        return 0;
    }

    /**
     * @param $url
     * @return bool|string
     * @throws GuzzleException
     */
    private function getPage($url)
    {
        $request  = $this->getRequest();
        $response = $this->client->request('GET', $url,
            [
                'query' => [
                    'q' => $request->get('keyword'),
                    'num' => config('search.search_amount')
                ],
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
                    'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Upgrade-Insecure-Requests' => 1
                ]
            ]
        );

        return $response->getBody()->getContents();
    }

    /**
     * @param ResultRequest $request
     * @return ResultRequest
     */
    public
    function setRequest(
        ResultRequest $request
    ) {
        return $this->request = $request;
    }

    /**
     * @return ResultRequest
     */
    public
    function getRequest()
    {
        return $this->request;
    }

}