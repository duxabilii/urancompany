<?php

namespace App\Services;


use App\Http\Requests\ResultRequest;
use App\Models\Result;
use Illuminate\Support\Facades\Auth;

class ResultService
{
    private $parser;

    public function __construct(ParserService $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @param ResultRequest $request
     * @return Result|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store(ResultRequest $request)
    {
        $attempts = 0;
        $success = false;
        do {
            try {
                $position = $this->parser->parse($request);
                $success = true;
            } catch (\Exception $e) {
                $attempts++;
                sleep(config('search.sleep'));
                continue;
            }
            break;
        } while ($attempts < config('search.retry_count'));

        if (!$success) {
            return false;
        }

        return Result::create([
            'domain' => $request->get('domain'),
            'keyword' => $request->get('keyword'),
            'position' => $position,
            'user_id' => Auth::user()->id
        ]);
    }
}