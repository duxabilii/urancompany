<?php

return [


    /*
     * URI for creating search request
     */
    'search_uri' => 'https://www.google.com.ua',
    /*
     * Results amount from Google
     * The allowed values are 10, 20, 30, 40, 50, and 100. Any other values for this field are ignored.
     */
    'search_amount' => 100,
    /*
     * Messages
     */
    'message' => [
        'position_found' => 'Domain "%s" WAS FOUND in Google search results by keyword "%s", position %d',
        'position_not_found' => 'Domain "%s" NOT FOUND in Google search results by keyword "%s"',
        'error_fetch' => 'Error fetching data from Google. Please, try again later'
    ],
    /*
     * Amount of retrying query to Google
     */
    'retry_count' => 3,
    /*
     * Seconds of pause after request
     */
    'sleep' => 2


];