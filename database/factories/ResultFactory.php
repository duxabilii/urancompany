<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Result::class, function (Faker $faker) {
    $datetime = $faker->dateTimeBetween('-3 days', 'NOW');
    return [
        'user_id' => 1,
        'domain' => $faker->url,
        'keyword' => $faker->word,
        'position' => $faker->numberBetween(0, 50),
        'created_at' => $datetime,
        'updated_at' => $datetime
    ];
});
