<?php

use App\Models\Result;
use App\Models\User;
use Illuminate\Database\Seeder;

class ResultsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::all()->first();
        factory(Result::class, 50)->create([
            'user_id' => $user->id
        ]);
    }
}
