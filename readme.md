# Test task for "Urancompany" ##

## Installation ##

1. `git clone https://duxabilii@bitbucket.org/duxabilii/urancompany.git urancompany.test`
2. `cd urancompany.test`
3. `composer install`
4. `cp .env.example .env` (Linux) or `copy .env.example .env` (Windows) 
5. `php artisan key:generate`
6. In the `.env` file fill in the `DB_HOST`, `DB_PORT`, `DB_DATABASE`, `DB_USERNAME`, and `DB_PASSWORD` options
7. `php artisan migrate`
8. `php artisan db:seed` (optionally)
9. After seeding you can login in using next credentials: Login: `admin@example.com`, Password: `secret` or register your own account