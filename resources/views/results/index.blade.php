@extends('layouts.app')

@section('content')
    <div class="col-md-10">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>
                    ID
                </th>
                <th>
                    Domain
                </th>
                <th>
                    Keyword
                </th>
                <th>
                    Position
                </th>
                <th>
                    Date of parsing
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($results as $result)
                <tr>
                    <td class="align-middle text-center">
                        <span class="badge badge-primary">{{ $result->id }}</span>
                    </td>
                    <td class="align-middle">
                        {{ $result->domain }}
                    </td>
                    <td class="align-middle">
                        {{ $result->keyword }}
                    </td>
                    <td class="align-middle text-center">
                        {{ $result->position === 0 ? 'Not found' : $result->position }}
                    </td>
                    <td class="align-middle text-center">
                        {{ $result->created_at }}
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>

        <div class="row justify-content-center">
            {{ $results->links() }}
        </div>
@endsection