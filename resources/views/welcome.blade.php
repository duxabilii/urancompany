@extends('layouts.app')

@section('content')
    <div class="col-md-10">
        <div class="card">
            <div class="card-header">Enter domain name and keyword for parsing</div>

            <div class="card-body">
                <form class="form-inline" method="POST" action="{{ route('results.store') }}">
                    <label class="sr-only" for="domain">Domain</label>
                    <input type="text" class="form-control mb-2 mr-sm-2" id="domain" name="domain"
                           placeholder="Enter domain name..." value="{{ old('domain') }}">

                    <label class="sr-only" for="keyword">Keyword</label>
                    <div class="input-group mb-2 mr-sm-2">
                        <input type="text" class="form-control" id="keyword" name="keyword"
                               placeholder="Enter keyword..." value="{{ old('keyword') }}">
                    </div>
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary mb-2">Submit</button>
                </form>

            </div>
@endsection