<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * The login form can be displayed.
     *
     * @return void
     */
    public function testLoginFormDisplayed()
    {
        $this->get('/login')
             ->assertStatus(200);
    }

    /**
     * A valid user can be logged in.
     *
     * @return void
     */
    public function testLoginAsValidUser()
    {
        $user = factory(User::class)->create();
        $this->post('/login',
            [
                'email' => $user->email,
                'password' => 'secret'
            ]
        )
             ->assertStatus(302);
        $this->assertAuthenticatedAs($user);
    }

    /**
     * An invalid user cannot be logged in.
     *
     * @return void
     */
    public function testLoginAsInvalidUser()
    {
        $user = factory(User::class)->create();
        $this->post('/login',
            [
                'email' => $user->email,
                'password' => 'notsecret'
            ]
        )
             ->assertSessionHasErrors();
        $this->assertGuest();
    }

    /**
     * A logged in user can be logged out.
     *
     * @return void
     */
    public function testLogoutAnAuthUser()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user)
             ->post('/logout')
             ->assertStatus(302);
        $this->assertGuest();
    }
}
