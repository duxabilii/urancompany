<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResultTest extends TestCase
{
    use DatabaseTransactions;

    public function testWelcomePage()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user)
             ->get('/')
             ->assertStatus(200);
    }

    public function testGoogleParse()
    {
        $user = factory(User::class)->create();
        $this->followingRedirects()
             ->actingAs($user)
             ->post('/results', [
                 'domain' => 'example.com',
                 'keyword' => 'Example',
             ])
             ->assertStatus(200);
    }
}